import os
import cv2
import sys
import csv
import time
import numpy as np
import argparse
from datetime import datetime
import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
from PIL import Image, ImageOps
from torch.utils.data import DataLoader, random_split
import torchvision.transforms as transforms
from models.Conv3D import r2plus1d_18
from dataset_sign_clip import Sign_Isolated
from validation_clip import val_epoch
from collections import OrderedDict
import albumentations as A

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', help='path to input video')
parser.add_argument('-o', '--output', help='path to output video')
parser.add_argument('-f', '--number_of_frames', dest='number_of_frames', default=32, type=int, help='number of frames to consider for each predicition')

args = vars(parser.parse_args())

class_names = {}
with open('/content/drive/MyDrive/bachelor-diploma-project/AuTSL/SignList_ClassId_TR_EN.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
        else:
            class_names[int(row[0])] = row[2]
            line_count += 1
    print(f'Processed {line_count} lines.')

val_data_path = "/content/Test/rgb-frames"
val_labels_path = "/content/Test/labels.csv"
phase = 'Test'

# Hyperparams
num_classes = 226
epochs = 100
batch_size = 1
learning_rate = 1e-3  #1e-3 Train, 1e-4 Finetune
weight_decay = 1e-4  #1e-4, default: 0
log_interval = 80
sample_size = 128
sample_duration = 32
attention = False
drop_p = 0.0
hidden1, hidden2 = 512, 256
num_workers = 6


transform = A.Compose([
    A.Resize(sample_size, sample_size, always_apply=True),
    A.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5],
    always_apply=True)
])
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Create model
model = r2plus1d_18(pretrained=True, num_classes=226)
checkpoint = torch.load(
        '/content/drive/MyDrive/pretrained-models/Conv3D/final_models_finetuned/rgb_final_finetuned.pth'
    )
new_state_dict = OrderedDict()
for k, v in checkpoint.items():
    name = k[7:]  # remove 'module.'
    new_state_dict[name] = v
model.load_state_dict(new_state_dict)
print(model)

model = model.to(device)
# Run the model parallelly
if torch.cuda.device_count() > 1:
    logger.info("Using {} GPUs".format(torch.cuda.device_count()))
    model = nn.DataParallel(model)

model.eval()

def crop(image, center, radius, size=512):
    scale = 1.3
    radius_crop = (radius * scale).astype(np.int32)
    center_crop = (center).astype(np.int32)

    rect = (max(0, (center_crop - radius_crop)[0]),
            max(0, (center_crop - radius_crop)[1]),
            min(512, (center_crop + radius_crop)[0]),
            min(512, (center_crop + radius_crop)[1]))

    image = image[rect[1]:rect[3], rect[0]:rect[2], :]

    if image.shape[0] < image.shape[1]:
        top = abs(image.shape[0] - image.shape[1]) // 2
        bottom = abs(image.shape[0] - image.shape[1]) - top
        image = cv2.copyMakeBorder(image,
                                   top,
                                   bottom,
                                   0,
                                   0,
                                   cv2.BORDER_CONSTANT,
                                   value=(0, 0, 0))
    elif image.shape[0] > image.shape[1]:
        left = abs(image.shape[0] - image.shape[1]) // 2
        right = abs(image.shape[0] - image.shape[1]) - left
        image = cv2.copyMakeBorder(image,
                                   0,
                                   0,
                                   left,
                                   right,
                                   cv2.BORDER_CONSTANT,
                                   value=(0, 0, 0))
    return image

selected_joints = np.concatenate(([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    [91, 95, 96, 99, 100, 103, 104, 107, 108, 111],
    [112, 116, 117, 120, 121, 124, 125, 128, 129, 132]), axis=0)

cap = cv2.VideoCapture(args['input'])
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
save_name = f"{args['input'].split('/')[-1].split('.')[0]}"
out = cv2.VideoWriter(f"{args['output']}/{save_name}.mp4", cv2.VideoWriter_fourcc(*'mp4v'), 30, (frame_width, frame_height))

npy = np.load(os.path.join(f"/content/Test/skeleton-keypoints/{save_name}.mp4.npy")).astype(np.float32)
npy = npy[:, selected_joints, :2]
npy[:, :, 0] = 512 - npy[:, :, 0]

xy_max = npy.max(axis=1, keepdims=False).max(axis=0,
                                             keepdims=False)
xy_min = npy.min(axis=1, keepdims=False).min(axis=0,
                                             keepdims=False)
assert xy_max.shape == (2, )
xy_center = (xy_max + xy_min) / 2 - 20
xy_radius = (xy_max - xy_center).max(axis=0)

frame_count = 0
total_fps = 0
sample_frames = []
sample_outputs = []
label = ''
while(cap.isOpened()):
    ok, frame = cap.read()
    if ok:
        start_time = time.time()
        image = frame.copy()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) # Convert frame to RGB
        # frame = frame/255
        frame = crop(frame, xy_center, xy_radius)
        frame = cv2.resize(frame, (256, 256))
        frame = frame[16:240, 16:240,:]
        frame = transform(image=frame)['image']
        sample_frames.append(frame)
        if len(sample_frames) == 32:
            with torch.no_grad():
                input_frames = np.array(sample_frames)
                input_frames = np.transpose(input_frames, (3, 0, 1, 2))
                input_frames = torch.tensor(input_frames, dtype=torch.float32)
                input_frames = x = torch.unsqueeze(input_frames, dim=0)
                input_frames = input_frames.to(device)
                output = model(input_frames)
                sample_outputs.append(output)
                if len(sample_outputs) == 32:
                    outputs = torch.mean(torch.stack(sample_outputs, dim=0), dim=0)
                    prediction = torch.max(outputs, 1)[1].detach().cpu().numpy()[0]
                    label = class_names[prediction].strip()
                    print(label)
                    sample_outputs.pop(0)
            end_time = time.time()
            fps = 1 / (end_time - start_time)
            total_fps += fps
            frame_count += 1
            wait_time = max(1, int(fps/4))
            cv2.putText(image, label, (30, 40), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 0, 0), 2, lineType=cv2.LINE_AA)
            sample_frames.pop(0)
            out.write(image)
            if cv2.waitKey(wait_time) &0xFF == ord('q'):
                break
    else:
        break

cap.release()
cv2.destroyAllWindows()

avg_fps = total_fps / frame_count
print(f"Average FPS: {avg_fps:.3f}")
