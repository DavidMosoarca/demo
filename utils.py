import csv
import albumentations as A

transform = A.Compose([
    A.Resize(256, 445, always_apply=True),
    # A.CenterCrop(224, 224, always_apply=True),
    A.Normalize(mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
                # max_pixel_value = 1.0,
                always_apply=True)
])

class_names = {}
with open('/content/drive/MyDrive/bachelor-diploma-project/AuTSL/SignList_ClassId_TR_EN.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
        else:
            class_names[int(row[0])] = row[2]
            line_count += 1
    print(f'Processed {line_count} lines.')
